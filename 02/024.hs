import Data.Char
fac::Int->Int
fac n=if (n==0) then 1 else n * (fac (n-1))

remo::Int->[Int]->(Int,[Int])
remo n (x:xs) | n==0      = (x,xs)
             | otherwise = let (y,ys)=remo (n-1) xs in (y,(x:ys))

level::(Int,[Int])->Int->(Int,[Int])
level (x,xs) n =(x `mod` a,xs++[x `div` a])
    where a=fac n

(0,decomp)=foldl level (1000000-1,[]) [9,8..0]

r'::([Int],[Int])->Int->([Int],[Int])
r' (xs,ys) n=(z:xs,zs) where (z,zs)=remo n ys

(inverseints,_)=foldl r' ([],[0..9]) $ decomp

result= reverse $ map intToDigit inverseints

main=print result
import Data.Char

fac::Integer->Integer
fac n=if (n==1) then 1 else n * (fac (n-1))

num=show $ fac 100

result=foldl (+) 0 $ map digitToInt num

main=print result
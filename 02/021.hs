propdiv' (a,b,c) | a==b = (a,b,c)
                | otherwise = propdiv' (a,b+1,c')
                    where c'=if a `mod` b==0 then c+b else c

propdiv a = c
    where (_,_,c)=propdiv' (a,1,0)

isfriend a = propdiv b==a && a/=b
    where b=propdiv a

friends=filter isfriend [2..10000]

result=foldl (+) 0 $ friends

main=print result
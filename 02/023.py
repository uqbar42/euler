def propdiv(a):
    result=0
    b=1
    while (2*b<=a):
        if (a % b ==0):
            result+=b
        b+=1
    return result

def abundant(a):
    return a>=12 and propdiv(a)>a

abundants=[abundant(x) for x in range(28123)]

def sumab(a):
    b=12
    while (2*b<=a):
        if (abundants[b] and abundants[a-b]):
            return True
        b+=1
    return False

sumall=0
x=1

while (x<=28123):
    if (not sumab(x)):
        sumall+=x
    x+=1

print(sumall)
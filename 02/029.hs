import Data.List

pw x n | n==1       = x
       | otherwise  = x * (pw x (n-1))

values=[ pw a b | a<-[2..100], b<-[2..100]]

result=length $ map head . group . sort $ values

main= print result
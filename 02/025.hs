nextfib (a:b:xs)=((a+b):a:b:xs)

digits n | n==0    =0
        |otherwise = 1+ digits (n `div` 10)


seqq=head $ dropWhile ((<1000).digits.head) $ iterate nextfib [1,1]

result=length seqq

main=print result
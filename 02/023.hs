propdiv' (a,b,c) | a==b = (a,b,c)
                | otherwise = propdiv' (a,b+1,c')
                    where c'=if a `mod` b==0 then c+b else c

propdiv a = c
    where (_,_,c)=propdiv' (a,1,0)

abundant a = a>=12 && a<(propdiv a)

allabundants=map abundant [0..28123]

isabundant n=allabundants!!n

sumab' a b | 2*b>a = False
        | otherwise = if ((isabundant b) && (isabundant (a - b))) then True else sumab' a (b+1)

sumab a=sumab' a 12

notsumab=filter (\t -> not (sumab t)) [0..28123]

result=foldl (+) 0 notsumab
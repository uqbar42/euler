numbers::[(Int,Int,Int)]

nextnum (a,b,c) | a==1 = (3,2,1)
                | c==4 = (a+b+2,b+2,1)
                | otherwise = (a+b,b,c+1)

numbers=iterate nextnum (1,0,1)

inSquare n (a,b,c)= b<n

square n = takeWhile (inSquare n) numbers

frst (n,_,_)=n

result=foldl (+) 0 $ map frst $ square 1001

main=print result
import Data.List
import Data.Maybe

nxt m n = if (n < m) then n*10 else 10*(n `mod` m)

cyc'::Int->(Int,Int,[Int])->(Int,Int,[Int])

cyc' m all@(n,count,reco) =if isJust k then (n,1+fromJust k,reco) else cyc' m (nxt m n,count+1,(n:reco))
    where k=elemIndex n reco
cyc m=if r==0 then 0 else k 
    where (r,k,_)=cyc' m (1,0,[])

allcyc=map (\t -> (t,cyc t)) [2..999]

getmax a@(_,x) b@(_,y) = if (x>y) then a else b

result=foldl getmax (0,0) allcyc

main = print x
    where (x,_)=result
    
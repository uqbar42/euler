import Data.List
import Data.Maybe

isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==(head $ dropWhile (<n) primes)

scope a b=length $ takeWhile isInPrimes $ map (\n ->n*(n+a)+b) [0..]

getmax a@(_,_,x) b@(_,_,y) = if (x>y) then a else b

scopes=[(a,b,scope a b) | a <-[-1000..1000], b<-[-1000..1000]]

result= a*b 
    where  
        (a,b,_)= foldl getmax (0,0,0) scopes

main = print result
factor::Int->Int->Bool
factor a b = (b `mod` a)==0

isPrime::[Int]->Int->Bool
isPrime (x:xs) t=if x*x>t
  then True
  else if factor x t then False else isPrime xs t
isPrime [] _=True

addPrimes:: Int -> [Int] -> Int -> [Int]
addPrimes top as b=ys
    where 
        x=6*b-1
        y=x+2
        xs=if x<top && (isPrime as x) then as++[x] else as
        ys=if y<top && (isPrime xs y) then xs++[y] else xs

nxt:: Int -> [Int]-> Int -> [Int]
nxt a as b = if b==0 
    then nxt a [2,3] 1 
    else 
        if 6*b-1>a 
            then as
            else
                nxt a xs (b+1) 
                where 
                    xs=addPrimes a as b

main = print $ foldl (+) 0 $ nxt 2000000 [] 0

{- 
multi::Int->[Int]->Int->Int->[Int]

multi max [] a b=multi max [b] a (b+a)
multi max (x:xs) a b=if b>max || x==a 
    then (x:xs)
    else if (b<x) 
        then (b:(multi max (x:xs)) a (b+a))
    else if b==x 
        then (x:multi max xs a (b+a))
    else (x:multi max xs a b)

fillMulti::Int->[Int]->Int->[Int]
fillMulti max xs a=multi max xs a (a+a)

allMulti::Int->[Int]->[Int]
allMulti max xs=foldl (fillMulti max) (fillMulti max [] 2) [3,5..max]
-}


twenties=["zero","one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty"]

tens=["zero","ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

letters x | x == 1000                     = "one thousand"
          | x <= 20                       = twenties!!x
          | (x<100) && (y==0)  = tens!!(x `div` 10)
          | (20<x) && (x<100)             = (letters (x - y)) ++ "-" ++ (letters y) 
          | z == 0              = (letters (x `div` 100)) ++ " hundred"
          | 100<x && x<1000               = (letters (x-z)) ++ " and " ++ (letters z) 
          where y=x `mod` 10
                z=x `mod` 100 

count x=length $ filter (\y -> not (y==' ' || y=='-')) $ letters x

main = print $ foldl (+) 0 $ map count [1..1000]
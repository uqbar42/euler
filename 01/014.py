def f(x):
    if x % 2 ==0:
        return x/2
    else:
        return x+x+x+1

def ch(x):
    if x<=1:
        return 1
    else:
        return 1+ch(f(x))
    
maxch=0
maxpoint=0

for x in range(1,999999):
    v=ch(x)
    if (v>maxch):
        maxch=v
        maxpoint=x

print(maxpoint)
print(maxch)
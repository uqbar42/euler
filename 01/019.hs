byMonth=[
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
    ]
leap n=if (n `mod` 400==0) || ((n `mod` 100)/=0 && (n `mod` 4)==0) then 1 else 0

sumMonth=scanl (+) 0 byMonth

next (a,c) y=(a+365+c,leap (y+1))
sumYear=scanl next (0,0) [1901..1999]

sumMonthYear (a,l)=map (+a) $ zipWith (+) sumMonth [0,0,l,l,l,l,l,l,l,l,l,l]

weekDay n = n `mod` 7

weekDay1st=weekDay (365+1)

firstMonthDays=sumYear >>= sumMonthYear

weekDay1stMonth=map (\x -> weekDay (x+weekDay1st)) firstMonthDays

result=length $ filter (==0) weekDay1stMonth

main=print result

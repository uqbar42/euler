pow::Integer->Integer
pow n=if n==0 then 1 else 2*pow (n-1)

fig'::Integer->Integer->Integer
fig' n x=if n<10
    then x+n
    else fig' (n `div` 10) (x+(n `mod` 10))

fig::Integer->Integer
fig n=fig' n 0

main= print $ fig $ pow 1000
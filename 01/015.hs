lat x y =
    if x==1 then y+1
    else if y==1 then x+1
    else (lat (x-1) y)+(lat x (y-1))

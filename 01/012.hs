--Number of divisors up to a limit
ndiv'::Int->Int->Int
ndiv' n 0=0
ndiv' n t | t>=1=(ndiv' n (t-1)) + if n `mod` t == 0 then 1 else 0

--Number of divisors
ndiv::Int->Int
ndiv n=ndiv' n n

cached_ndiv::[Int]
cached_ndiv=map ndiv [0..]

-- Decomposition in 2 factors of a triangular number
factri::Int->(Int,Int)
factri n=if (n `mod` 2==0) then (n `div` 2, n+1) else (n, (n+1) `div` 2)

-- Number of factors of the n-th triangular number
ndivtri::Int->Int
ndivtri n=cached_ndiv!!a * cached_ndiv!!b
    where (a,b)=factri n

-- The solution
result::[Int]
result=take 1 $ map (\y -> (y*(y+1)) `div` 2) $ filter (\x ->((ndivtri x)>500)) [1..]

main=print result

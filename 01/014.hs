
f::Int->Int
f n=if n `mod` 2==0 then n `div` 2 else n+n+n+1

chain::Int->Int
chain n= if (n<=1) then 1 else 1+(chain (f n))

max2nd::(Int,Int)->(Int,Int)->(Int,Int)
max2nd a@(_,b) a'@(_,b')=if b>=b' then a else a'

numcached=2999999

chain''::Int->Int
chain'''::Int->Int
cache::[Int]

cache=map chain'' [0..numcached]

chain'' n=if (n<=1) then 1 else 1+(chain''' (f n))
chain''' n = if (n<=numcached) then cache!!n else chain'' n

result=foldl max2nd (0,0) $ map (\t -> (t,cache!!t)) [1..999999]

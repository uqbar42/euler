
rev' a b=if b==0 then a else rev' (a*10+(b `mod` 10)) (b `div` 10)
rev b =rev' 0 b

topal' n a=if n==0 then False else let y=rev a in if y==a then True else topal' (n-1) (a+y)
topal a=topal' 50 (a+(rev a))

lyc a = not (topal a)

result=length $ filter lyc [1..9999]

main = print result
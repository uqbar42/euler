from math import comb


def big(i):
    result=0
    for j in range(1,i):
        if comb(i,j)>1000000:
            result+=1
    return result

total=0
for i in range(1,101):
    x=big(i)
    total+=x
    print(f' {i} => {x}')


print(total)



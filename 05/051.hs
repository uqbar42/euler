import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==h
            where (h:_)= dropWhile (<n) primes

fig m x=if m==0 then x `mod` 10 else fig (m-1) (x `div` 10)

figure Nothing a=Just a
figure (Just a) b=if a==b then Just a else Nothing

still Nothing a=True
still (Just a) b=a==b

equiv::Int->Int->Int->Int->Int->Int->Maybe Int->Maybe Int->Maybe ((Int,Int),(Int,Int))
equiv a b r oa ob p ga gb=if a<p && b<p then Just ((r,if (fromJust gb==0) then oa `div` (fromJust ga) else ob `div` (fromJust gb)),(fromJust ga,fromJust gb)) else
                let 
                aa=(a `div` p) `mod` 10
                bb=(b `div` p) `mod` 10
                in
                    if aa==bb 
                        then equiv a b (r+p*aa) oa ob (10*p) ga gb
                        else if still ga aa && still gb bb 
                            then equiv a b (r) (oa+p*aa) (ob+p*bb) (10*p) (figure ga aa) (figure gb bb)
                            else Nothing
common::(Int,Int)->[(Int,Int,Int)]
common (a,b)=
    let w=equiv a b 0 0 0 1 Nothing Nothing
    in if isJust w 
        then
            let ((x,y),(aa,bb))=fromJust w
            in [(x,y,aa),(x,y,bb)]
        else
            []

pow10 n=if n==0 then 1 else 10*pow10 (n-1)

level n=dropWhile (<p) $ takeWhile (<pp) primes
    where
        p=pow10 (n-1)
        pp=10*p

zipWithOthers []=[]
zipWithOthers (a:as)=[(a,t) | t<-as] ++ zipWithOthers as

groupBy2 (a,b,_) (c,d,_) = a==c && b==d

processLevel n = map head $ group $ sort $ (zipWithOthers (level n) >>= common)

seekLevel n = map (\all@((a,b,c):_) -> ((a,b),length all)) $ groupBy groupBy2 $ processLevel n

max3 a@(_,a2) b@(_,b2)= if a2>=b2 then a else b

maxLevel n= foldl max3 ((0,0),0) $ seekLevel n

allbylevel=map maxLevel [1..]


equivFirst::Int->Int->Int->Int->Int->Maybe Int->Maybe Int->Maybe (Int,Int)
equivFirst a b r ox p ga gb=if a<p && b<p then Just (r,ox) else
                let 
                aa=(a `div` p) `mod` 10
                bb=(b `div` p) `mod` 10
                in
                    if aa==bb 
                        then equivFirst a b (r+p*aa) ox (10*p) ga gb
                        else if still ga aa && still gb bb 
                            then equivFirst a b r (ox+p) (10*p) (figure ga aa) (figure gb bb)
                            else Nothing

commonFirst::(Int,Int)->[(Int,Int)]
commonFirst (a,b)=
    let w=equivFirst a b 0 0 1 Nothing Nothing
    in if isJust w 
        then 
            [fromJust w]
        else
            []

--processLevel n = map head $ group $ sort $ (zipWithOthers (level n) >>= common)
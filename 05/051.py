from math import comb

end=1000000
isPrime=[True]*end

for x in range(2,1+end//2):
    y=x+x
    while y<end:
        isPrime[y]=False
        y+=x

primes=[]
for x in range(2,end):
    if (isPrime[x]): primes.append(x)

target=8
targetPairs=comb(target,2)
for n in range(2,target+1):
    c=comb(n,2)
    print(f'{n} => {c}')

def isNothingOrEqual(x,y):
    if x is None:
        return True
    else:
        return x==y

def diff(a,b):
    r=0
    f=1
    fa=None
    fb=None
    sim=0
    while (a>=f or b>=f):
        aa=(a//f) % 10
        bb=(b//f) % 10
        #print(f' {aa} {bb}')
        if (aa==bb):
            sim+=aa*f
        else:
            if isNothingOrEqual(fa,aa) and isNothingOrEqual(fb,bb):
                fa=aa
                fb=bb
                r+=f
            else:
                return None
        f=f*10
    return (sim,r)

def pow10(n):
    if (n==0): return 1
    return 10*pow10(n-1)

def similarPairs(n):
    result=[]
    x=pow10(n-1)
    xx=10*x

    while x<xx:
        if isPrime[x]:
            y=x+1
            while y<xx:
                if isPrime[y]:
                    sim=diff(x,y)
                    if not sim is None:
                        yield sim
                        #result.append(sim)
                y+=1
        x+=1

def processLevel(n):
    sz=0
    ct=0
    oc={}
    for x in similarPairs(n):
        nb=0
        if x in oc:
            nb=oc[x]
        nb+=1
        if nb==sz:
            ct+=1
            print(f' {sz} ({ct})')
        elif nb>sz:
            sz=nb
            ct=1
            print(f' {sz} ({ct})')
            if targetPairs<=sz:
                (a,b)=x
                print(f'Solution: {a} + {b} f')
                if b>a:
                    start=1
                else:
                    start=0
                for f in range (start,10):
                    y=a+f*b
                    if isPrime[y]:
                        print(y,end=",")
                print()
                exit()
        oc[x]=nb
    return oc


def scanLevel(n):
    print(f'Level {n}')
    x=processLevel(n)
    for a,b in x.items():
        if b>=28:
            print(f'Result: {b} => {a}')


for n in range(1,7):
    scanLevel(n)
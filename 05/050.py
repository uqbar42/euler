end=1000000
isPrime=[True]*end

for x in range(2,1+end//2):
    y=x+x
    while y<end:
        isPrime[y]=False
        y+=x

primes=[]
for x in range(2,end):
    if (isPrime[x]): primes.append(x)

mx=0
mxn=0

for x in primes:
    s=0
    n=0
    top=0
    tops=0
    topn=0
    for y in range(x,end):
        if (y+s>=end): break
        if isPrime[y]:
            s=s+y
            n+=1
            if isPrime[s]:
                top=y
                tops=s
                topn=n+1

    if (topn>mxn):
        mx=tops
        mxn=topn

print(mx)
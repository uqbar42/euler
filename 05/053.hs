lv::Integer->[Integer]
lv n | n==1 =[1,1]
     | otherwise = zipWith (+) (0:ll) (ll++[0])
                    where ll=lv (n-1)

big n=length $ filter (>1000000) $ lv n

result=foldl (+) 0 $ map big [1..100]
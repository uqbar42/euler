import Data.List

splt'::Int->[Int]
splt' n=if n==0 then [] else (n `mod` 10:splt' (n `div` 10))
splt = sort.splt'

condition a = splt (2*a)==x && splt (3*a)==x && splt (4*a)==x && splt (5*a)==x && splt (6*a)==x
    where x=splt a


import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==h
            where (h:_)= dropWhile (<n) primes

zipPrimes _ []=[]
zipPrimes [] _=[]
zipPrimes aa@(a:as) bb@(b:bs)=if a>=b then (True:zipPrimes as bs) else (False:zipPrimes as bb)


step (a,s,k) p = (a,s+p,k+1)

chunks []=[]
chunks (x:xs) = scanl step (x,x,1) xs

primeSeq ps = filter (\(_,p,_)->elem p ps) $ chunks ps

primesUnder n=takeWhile (<n) primes

sequencesUnder n= (tails (primesUnder n) >>= primeSeq)

getmax xs=foldl (\a1@(_,_,c1) a2@(_,_,c2) -> if c1>c2 then a1 else a2) (0,0,0) xs

test1000=getmax $ sequencesUnder 1000


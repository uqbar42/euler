mcd a b=if d==0 then b else mcd b d
  where d=a `mod` b

fac a b = b `div` d
  where d = mcd a b

addfac a b = a*d
  where d=fac a b
  
main = print $ foldl addfac 1 [1..20]

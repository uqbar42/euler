n::Int=600851475143
h (a,b) = if a `mod` b ==0 
  then (a `div` b,b)
  else (a,b+2)

ceilx::Int->(Int,Int)->(Int,Int)
ceilx a (x,y) = (x,if (y<a) then (y+x) else y)

step::(Int,Int)->(Int,[(Int,Int)],Bool)->(Int,[(Int,Int)],Bool)

step (x,y) (a,xs,v) = (a, ((x,t):xs),v || t==a)
  where
    t=if y<a then y+x else y
    
pass a xs= if v then ys else ((a,a):ys)
  where (_,ys,v)=foldr step (a,[],False) xs

prim::Int->[(Int,Int)]
prim 0=[]
prim n=pass m (prim (n-1))
  where m=2*n+1
  
extract::Int->Int->Int
extract a b = if (a `mod` b == 0) then (extract (a `div` b) b) else a

reduce :: (Int,Int,[(Int,Int)])->(Int,Int,[(Int,Int)])

reduce (a,b,xs)=if v 
  then (a,p,ys) 
  else ((extract a p),p,(p,p):ys)
  where 
    p=b+2
    (_,ys,v)=foldr step (p,[],False) xs

full (a,b,xs)=if b*b>a
  then a
  else full (reduce (a,b,xs))

main = print $ full (n,1,[])

import Data.List
palindrome :: Int -> Bool

split 0=[]
split n=(n `mod` 10):(split (n `div` 10))

palindrome a = (xs == (reverse xs))
  where xs = split a
  
combi a b = [(a+9,b+1),(a+3,b+3),(a+1,b+9)] 

cmb::Int -> Int -> [(Int,Int)] ->[(Int,Int)]
cmb a b  = map (\(x,y)->(a+x,b+y))

  
fac10::Int->Int->(Int,Int,Int)
fac10 a b = ((a*b) `mod` 10,a,b)

cmp1 :: (Int,Int,Int) -> (Int,Int,Int) -> Ordering

cmp1 (a,_,_) (b,_,_)=compare a b

pairs=[fac10 a b | a<-[0..9],b<-[0..9]]

shift1::Int->(Int,Int,Int)->(Int,Int,Int)
shift1 n (a,b,c) = ((a+n) `mod` 10,b,c)
shifted n= map (\(_,x,y)->(x,y)) $ reverse $ sortBy cmp1 (map (shift1 n) pairs)

dec (a,b) = ((a*b) `div` 10) `mod` 10

--take 5 (shifted 0)

f::Int->(Int,Int)->Int->Int->(Int,Int,Int)
f n (a,b) x y=((u*v) `mod` (10*n),u,v)
  where 
    u=x*n+a
    v=y*n+b

g::Int->(Int,Int)->Int->Int->(Int,Int,Int)
g n (a,b) x y=(u*v,u,v)
  where 
    u=x*n+a
    v=y*n+b

strip (_,a,b)=(a,b)

lev::Int->(Int,Int)->[(Int,Int)]
lev n w=map strip $ reverse $ sortBy cmp1 [ f n w x y | x<-[0..9],y<-[0..9]]

lev'=lev 1 (0,0)

lev''=lev' >>= (lev 10)

levPal n w = [ (a,b,c) | x<-[0..9],y<-[0..9], let (a,b,c)= g n w x y, palindrome a, a>100000]

main = print $ takeWhile (\(x,_,_) -> (x `mod` 10) == 9) (lev'' >>= (levPal 100))
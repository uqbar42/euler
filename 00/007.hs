-- ADD method
step::(Int,Int)->(Int,[(Int,Int)],Bool)->(Int,[(Int,Int)],Bool)

step (x,y) (a,xs,v) = (a, ((x,t):xs),v || t==a)
  where
    t=if y<a then y+x else y
    
pass (b,xs)= if v 
  then pass (b+2,ys) 
  else (a,(a,a):ys)
  where 
    a=b+2
    (_,ys,v)=foldr step (a,[],False) xs
nxt::[(Int,Int)]->[(Int,Int)]
nxt []=[(3,6)]
nxt (x:xs)=ys
  where
  (a,_)=x
  (_,ys)=pass (a,x:xs)
  
applyN = (foldr (.) id.) . replicate

primes=applyN 9999 nxt [(3,3),(2,4)]

-- modulo method

nxt'::[Int]->Int->Int
factor::Int->Int->Bool
factor a b = (b `mod` a)==0

isPrime::[Int]->Int->Bool
isPrime (x:xs) t=if x*x>t
  then True
  else if factor x t then False else isPrime xs t
isPrime [] _=True

nxt' as b = if b==2 then 3 
  else
  if isPrime as b then b 
  else nxt' as (b+2)

iter :: (Int,[Int])->(Int,[Int])
iter (a,xs)=(b,ys)
  where
    b=nxt' xs (a+2)
    ys=xs++[b]
(_,primes')=applyN 10000 iter $ (1,[2])

main=print $ last primes'
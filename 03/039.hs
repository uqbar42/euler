trg a b c =a*a+b*b==c*c

perim=1000

tr p =[(a,b) | a<-[1..p `div` 3], b<-[a..(p-a) `div` 2 ],trg a b (p-a-b) ]

levels=map (\t ->(t,length (tr t))) [3..1000] 

mx a1@(_,n1) a2@(_,n2)= if (n1>n2) then a1 else a2

result=foldl mx (0,0) levels

main=print result

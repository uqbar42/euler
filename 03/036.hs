decay' k (n,x)=(a,x*k+b)
    where (a,b)=quotRem n k

invert' k all@(n,_) | n==0 = all
              | otherwise = invert' k (decay' k all)
invert k n = m
    where (_,m)=invert' k (n,0)

toBin1 (n,xs) = (a,(b:xs))
    where (a,b)=quotRem n 2

toBin' all@(n,_)= if (n==0) then all else toBin' (toBin1 all)

toBin n = m
    where (_,m)=toBin' (n,[])

palindromic n = (n==invert 10 n) && (n==invert 2 n)

palindromics=filter palindromic [1..999999]

result=foldl (+) 0 palindromics

main=print result
import Data.List
import Data.Maybe

digits' (n,a)=if n==0 then (n,a) else digits' (n `div` 10, a+1)
digits n=a
    where (_,a)=digits' (n,0)

pow10 n=if n==0 then 1 else 10*(pow10 (n-1))

rotate pow n=a + b*pow
    where (a,b)=quotRem n 10

isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==(head $ dropWhile (<n) primes)

allPrimes d pow n=all isInPrimes $ take (d+1) $ iterate (rotate pow) n 

listCircularPrimes d=filter (allPrimes d pow) [a..b]
    where
        pow=pow10 (d-1)
        a=pow+1
        b=(pow*10)-1

allUnderMillion=[1..6] >>= listCircularPrimes
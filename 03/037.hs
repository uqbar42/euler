import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==(head $ dropWhile (<n) primes)

pow10 n=if n==0 then 1 else 10*(pow10 (n-1))

nextr _ n=n `div` 10

nextl pow n= n - (n `div` pow)*pow

primesr pow n= isInPrimes n && (n<10 || primesr (pow `div` 10) (nextr pow n) )
primesl pow n= isInPrimes n && (n<10 || primesl (pow `div` 10) (nextl pow n) )

primesboth pow n=(primesr pow n)&&(primesl pow n)

primesdigit n= filter (primesboth pow) [a,a+2..b]
    where 
        pow=pow10 (n-1)
        a=pow+1
        b=pow*10-1

allprim=take 11 $ [2..] >>= primesdigit

result=foldl (+) 0 allprim

main=print result
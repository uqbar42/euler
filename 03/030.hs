decay' (n,x)=(n `div` 10,x+ m*m*m*m*m)
    where m=n `mod` 10

decay all@(n,_)=if (n==0) then all else decay (decay' all)

sum5th n = m
    where (_,m)=decay (n,0)

maxval=sum5th 999999

selfsum n= n == sum5th n

allselfsum=filter selfsum [2..maxval]

result=foldl (+) 0 allselfsum

main=print result

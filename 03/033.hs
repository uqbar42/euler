fracs= [ (a,b) | x<-[0..9], y<-[0..9], z<-[0..9], let a=x*10+y, let b=x*10+z, a>9 && b>9 && a<b && a*z==b*y ]
    ++ [ (a,b) | x<-[0..9], y<-[0..9], z<-[0..9], let a=x+y*10, let b=x*10+z, a>9 && b>9 && a<b && a*z==b*y ]
    ++ [ (a,b) | x<-[0..9], y<-[0..9], z<-[0..9], let a=x*10+y, let b=x+z*10, a>9 && b>9 && a<b && a*z==b*y ]
    ++ [ (a,b) | x<-[1..9], y<-[0..9], z<-[0..9], let a=x+y*10, let b=x+z*10, a>9 && b>9 && a<b && a*z==b*y ]

mcd::Int->Int->Int

mcd x y | y==0 = x
        | x==0 = y
        | otherwise = mcd y (x `mod` y)

mul (a1,b1) (a2,b2)=(a `div` d,b `div` d)
    where 
        a=a1*a2
        b=b1*b2
        d=mcd a b

result=foldl mul (1,1) fracs

main=print result
fac n|n==0 = 1
    |otherwise = n*(fac (n-1))

decay' (n,m)=(n `div` 10, m+(fac (n `mod` 10)))
decay all@(n,_) = if n==0 then all else decay (decay' all)

facsum n=m
    where (_,m)=decay (n,0)

maxval=99999 -- arbitrary!

numbers=filter (\t->t==facsum t) [3..maxval]
result=foldl (+) 0 numbers
main=print result
end=1000000
isPrime=[True]*end


for x in range(2,1+end//2):
    y=x+x
    while y<end:
        isPrime[y]=False
        y+=x

def pow10(n):
    if (n==0):
        return 1
    return 10*pow10(n-1)

def rotate(pow,n):
    q,mod=divmod(n,10)
    return q+mod*pow

def cyclicPrime(digits,pow,n):
    i=0
    x=n
    while i<digits:
        if not isPrime[x]:
            return False
        i+=1
        x=rotate(pow,x)
    return True

print(cyclicPrime(3,100,197))
print(cyclicPrime(5,10000,11939))

def allCyclicPrimes(digits):
    result=[]
    pow=pow10(digits-1)
    a=pow+1
    b=10*pow
    for n in range(a,b):
        if cyclicPrime(digits,pow,n):
            result.append(n)
    return result

all=[]
for i in range(1,7):
    l=allCyclicPrimes(i)
    print(str(i) + '  ' + ','.join(str(x) for x in l))
    all=all+l
    
print(all)

print(len(all))

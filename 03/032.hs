import Data.List

oneToInt ((a:as),i) = (as,i*10+a)

toInt' (as,i)= if as==[] then i else toInt' (oneToInt (as,i))

toInt as=toInt' (as,0)

toFig' all@(i,as)=if i==0 then as else toFig' (i `div` 10, (i `mod` 10:as))

toFig i=toFig'(i,[])

disjoint a b=(filter (\t -> (elem t a)) b)==[]

rmv xs ys=filter (\t->not (elem t xs)) ys

rmv1 x ys=filter (/=x) ys

seqs'::[Int]->[[Int]]
seqs' xs =[[]] ++ [ [t] ++ ys | t<-xs, ys<-seqs'(rmv1 t xs), length ys < 4 ]

seqs = tail.seqs'

figs::[Int]=[1,2,3,4,5,6,7,8,9]

toBeChecked=[ (a,b) | a<-seqs figs, b<-seqs (rmv a figs)]

isPandig (a,b)= (sort c)==(rmv a (rmv b figs))
    where
        ia=toInt a
        ib=toInt b
        ic=ia*ib
        c=toFig ic
        
pandigSeqs=filter isPandig toBeChecked
pandigProds=map (\(a,b) -> (toInt a)*(toInt b)) pandigSeqs
result=foldl (+) 0 $ map head $ group $ sort $ pandigProds

main=print result
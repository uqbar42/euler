coins::[Int]=[1,2,5,10,20,50,100,200]

rcoins=reverse coins

candidates::Int->Int->[Int]
candidates n x=[t | t<-[0..n], x*t<=n]

splt::(Int,[(Int,Int)],[Int])->[(Int,[(Int,Int)],[Int])]
splt (n,a,(x:xs))=if x==1 then [(0,((n,1):a),[])] else [(pend,(if t==0 then a else (t,x):a),if pend==0 then [] else xs) | t<-candidates n x, let pend=n-x*t]

fullsplt::(Int,[(Int,Int)],[Int])->[(Int,[(Int,Int)],[Int])]
fullsplt (n,a,c) = if n==0 || c==[] then [(n,a,c)] else (splt (n,a,c) >>= fullsplt)

result=length $ fullsplt (200,[],rcoins)

main=print result
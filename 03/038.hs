import Data.List

len' all@(a,b)=if a==0 then all else len' (a `div` 10,b+1)

len a=b
    where (_,b)=len' (a,0)

splt' all@(a,xs)=if (a==0) then all else splt' (b,(y:xs))
    where (b,y)=quotRem a 10

splt a=b
    where (_,b)=splt' (a,[])

pow10 n=if n==0 then 1 else 10* pow10 (n-1)

multi l n = map (*n) [1..l]

cat'::([Int],Int)->Int
cat' (as,n)=if as==[] then n else 
        let (a:asx)=as
            l=len a
            pow=pow10 l 
        in cat' (asx, n * pow + a)

cat xs=cat' (xs,0)

ispandigitalset' xs=[1..9]==sort xs

pandigital' n xs=if n==0 then ispandigitalset' xs else
                let (m,r)=quotRem n 10
                in pandigital' m (r:xs)

pandigital n=pandigital' n []

level n= filter pandigital $ takeWhile (\t -> len t <=9) $ map (cat.(multi n)) [1..]

alllevels=[2..9] >>= level

result=foldl max 0 alllevels
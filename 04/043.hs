digits=[1..9]
rdigits=reverse digits

step::[Int]->([Int],[Int])->[([Int],[Int])]

step prev (prefix,values)=if values==[] then [] else
        let (x:xs)=values
        in ((prefix++[x],prev++xs):step (prev++[x]) (prefix,xs))

parts::([Int],[Int])->[[Int]]

parts (prefix,values)=if values==[] then [prefix] else 
        (step [] (prefix,values)) >>= parts

remove n []=[]
remove n (x:xs)=if (x==n) then xs else (x:remove n xs)

level n=parts([n],remove n [0..9])

allpandig=[1..9] >>= level

sq m n=(10*m+n)

toInt xs=foldl sq 0 xs

sub n xs=foldl sq 0 $ take 3 $ drop (n-1) xs

clause n d xs=(sub n xs) `mod` d==0

condition xs=clause 2 2 xs && clause 3 3 xs && clause 4 5 xs && clause 5 7 xs && clause 6 11 xs && clause 7 13 xs && clause 8 17 xs

testseq=[1,4,0,6,3,5,7,2,8,9]

sequences=filter condition $ allpandig

result=foldl (+) 0 $ map toInt sequences

main=print result
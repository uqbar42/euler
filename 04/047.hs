import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

has n []=False
has 1 (x:xs)=True
has n (x:xs)=has (n-1) xs

#primeDivisors n = filter (\t -> n `mod` t==0) $ takeWhile (\t -> t <= n) $ primes

appendIfDifferent [] n= [n]
appendIfDifferent all@(a:_) n= if a==n then all else (n:all)

listPrimeDivisors' n r (x:xs)=
    if (x>n) then 
        r
    else
        if (n `mod` x)/=0 
        then 
            listPrimeDivisors' n r xs
        else
            listPrimeDivisors' (n `div` x) (appendIfDifferent r x) (x:xs)

listPrimeDivisors n=listPrimeDivisors' n [] primes

condition n= has 4 $ listPrimeDivisors n

four n=[n,n+1,n+2,n+3]

numbers=filter condition [3..]

nxt (x,y) a=if (a==x+y) then (x,y+1) else (a,1)

result= take 1 $ filter (\(a,b)->b==4) $ scanl nxt (0,0) numbers

main=print result
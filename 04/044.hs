nth n=n*(n+n+n-1) `div` 2

pentagonals=map nth [1..]

pentagonal::Int->Bool
pentagonal x= f==x
    where f=head $ dropWhile (<x) pentagonals

delta = zipWith (\a b -> (b,a-b)) (drop 1 pentagonals) pentagonals

level n = map (\(a,b)->(a,a+n,n,a+a+n)) $ filter (\(a,b) -> pentagonal (a+n) && pentagonal (a+a+n)) $ takeWhile (\(a,b) -> b<=n) delta

allpairs=pentagonals >>= level

result=take 1 $ allpairs

main=print result

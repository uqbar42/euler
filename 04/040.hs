splt' all@(a,xs)=if (a==0) then all else splt' (b,(y:xs))
    where (b,y)=quotRem a 10

splt a=b
    where (_,b)=splt' (a,[])

seqn=[1..] >>= splt

factor (pow,val)=if (pow==0) then val else factor (pow `div` 10, (seqn!!(pow-1)) * val)

result=factor (1000000,1)

main=print result
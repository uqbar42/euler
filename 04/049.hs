import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

splt'::Int->[Int]
splt' n=if n==0 then [] else (n `mod` 10:splt' (n `div` 10))
splt = sort.splt'

candidates=filter (>1000) $ takeWhile (<10000) primes

f x=let spltx=splt x in (\t -> splt t==spltx)

sets= filter (\t -> length t>=3) $ map (\x-> filter (f x) candidates) candidates

hasnext _ []=Nothing
hasnext _ [x]=Nothing
hasnext n (x:xs)=if elem (x+x-n) xs then Just (n,x,x+x-n) else hasnext n xs

hasseq []=[]
hasseq [x]=[]
hasseq (x:xs)=(map fromJust $ filter (isJust) $ (map (hasnext x) (tails xs))) ++ hasseq xs

seqs=sets >>= hasseq

otherseqs=filter (\(t,_,_) -> t /= 1487) $ seqs

result=take 1 otherseqs

main=print result
triangle n=n*(n+1) `div` 2
pentagonal n=n*(n+n+n-1) `div` 2
hexagonal n=n*(n+n-1)

isTriangular k = (sq*sq==disc) && (k==triangle n)
    where 
        disc=1+8*k
        sq=floor (sqrt (fromIntegral disc))
        n=(sq - 1) `div` 2

isPentagonal k = (sq*sq==disc) && (k==pentagonal n)
    where 
        disc=1+24*k
        sq=floor (sqrt (fromIntegral disc))
        n=(sq + 1) `div` 6

result=take 3 $ filter (\t->isTriangular t && isPentagonal t) $ map hexagonal [1..]

main=print result
import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==(head $ dropWhile (<n) primes)

twiceasquare x= x==2*sq*sq
    where
        sq=floor (sqrt (fromIntegral (x `div` 2)))

condition' n p=twiceasquare (n-p)

condition n = any (condition' n) $ takeWhile (<n) primes

nextChunk n mx=if (n>=mx) then [] else (n:(nextChunk (n+2) mx))

nextNonPrime (n,(x:xs))=if (n==x) then nextNonPrime (n+2,xs) else Just (n,(n+2,(x:xs)))

nonPrimes=unfoldr nextNonPrime (3,drop 1 primes) 

result=take 1 $ filter (\t -> not (condition t)) $ nonPrimes

main=print result
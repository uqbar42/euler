import math
def pentagonal(n):
    return n*(n+n+n-1)//2

def pentagonals():
    n=1
    while True:
        yield pentagonal(n)
        n+=1

cacheSize=99999999
isPentagonalCached=[False]*cacheSize
for n in pentagonals():
    if n>=cacheSize: break
    isPentagonalCached[n]=True

def isPentagonal(k):
    if k<cacheSize: return isPentagonalCached[k]
    disc=1+24*k
    sq=math.floor(math.sqrt(disc))
    if not sq*sq==disc:
        return False
    return pentagonal((1+sq)//6)==k

for n in range(1000):
    p1=isPentagonalCached[n]
    p2=isPentagonal(n)
    if (p1!=p2):
        print(f'{n} => {p1} {p2}')

def level(l):
    n=1
    while(True):
        a=pentagonal(n)
        after=pentagonal(n+1)
        if (a+l<after):
            break
        b=a+l
        if (isPentagonal(b)):
            #print(f"Level {l} / {a} {b}")
            yield a+b
        n+=1


for l in pentagonals():
    #print(f"Level {l}")
    n=0
    last=0
    for d in level(l):
        n+=1
        last=d
        if isPentagonal(d):
            print("* ",end="")
            print(f'{l} {d}')
            exit()
    print(f'Level {l} {n} {last}')
    #if l>100:  exit()

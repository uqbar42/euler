import Data.List
import Data.Maybe
isPrime::[Int]->Int->Bool
isPrime xs n =if n==2 then True else isNothing fnd
    where
        ys=takeWhile (\x ->x*x<=n) xs
        fnd=find (\x -> n `mod` x ==0) ys

primes::[Int]
primes=filter (isPrime primes) [2..]

isInPrimes::Int->Bool
isInPrimes n = n==(head $ dropWhile (<n) primes)

ispandigitalset' xs=[1..9]==sort xs

pandigital' n xs=if n==0 then ispandigitalset' xs else
                let (m,r)=quotRem n 10
                in pandigital' m (r:xs)
pandigital n=pandigital' n []

digits=[1..9]
rdigits=reverse digits

step::[Int]->([Int],[Int])->[([Int],[Int])]

step prev (prefix,values)=if values==[] then [] else
        let (x:xs)=values
        in ((prefix++[x],prev++xs):step (prev++[x]) (prefix,xs))

parts::([Int],[Int])->[[Int]]

parts (prefix,values)=if values==[] then [prefix] else 
        (step [] (prefix,values)) >>= parts

toInt' [] y=y
toInt' (x:xs) y=toInt' xs (y*10+x)
toInt a = toInt' a 0

level xs=map toInt $ parts ([],xs)

allpandigs=(tails rdigits) >>= level

prime' n m=if m*m>n 
            then True 
            else
                if n `mod` m==0 
                    then False
                    else
                        prime' n (m+2)

prime n=(n `mod` 2/=0) && prime' n 3

result=take 1 $ filter prime allpandigs

main=print result